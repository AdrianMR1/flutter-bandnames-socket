import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:band_names/services/socket_service.dart';

class StatusPage extends StatelessWidget {
  const StatusPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    final socketService = Provider.of<SocketService>(context);
   

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Server Status: ${socketService.serverStatus}')
          ],
        )
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.message) ,
        onPressed: (){
        //aqui viene la tarea 
        //emitr un mapa 
        //{nombre: flutter,mensaje: 'Hola desde flutter'}
         socketService.socket.emit('emitir-mensaje',{
           'nombre': 'Flutter',
           'mensaje': 'Hola desde flutter'
           });

      }),
    );
  }
}